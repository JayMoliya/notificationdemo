package com.example.notificationdemo

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val CHANNEL_NAME = "channelName"
    val CHANNEL_ID = "channelId"
    val NOTIFICATION_ID = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        notification_button.setOnClickListener {

            createNotification()
        }

    }

    private fun createNotification() {

       val intent = Intent(this,MainActivity::class.java)
        intent.apply {
            action = Intent.ACTION_VIEW
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }

        val pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this,CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_account)
            .setContentTitle("Notification Title")
            .setContentText("This is Content Text of Notification")
            .setContentIntent(pendingIntent)
            .setVibrate(longArrayOf(500,500,500))
            .build()

        with(NotificationManagerCompat.from(this)){
            // to check if android version is greater than oreo(API 26) or not
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                val notificationChannel = NotificationChannel(CHANNEL_ID,CHANNEL_NAME,NotificationManager.IMPORTANCE_HIGH)
                createNotificationChannel(notificationChannel)
            }

            notify(NOTIFICATION_ID,builder)
        }
    }
}